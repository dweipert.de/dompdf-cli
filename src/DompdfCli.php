<?php

namespace DompdfCli;

use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\SingleCommandApplication;

class DompdfCli
{
  /**
   * DompdfCli constructor.
   */
  public function __construct()
  {
    $application = new SingleCommandApplication();

    $application = $this->configure($application);
    $application->setCode([$this, 'execute']);

    $application->run();
  }

  /**
   * Configure the Application
   */
  public function configure(SingleCommandApplication $application): SingleCommandApplication
  {
    $application->addArgument('input', InputArgument::REQUIRED, 'Path to html input file');
    $application->addArgument('output', InputArgument::REQUIRED, 'Path to pdf output file');

    $application->addOption('options', null, InputOption::VALUE_REQUIRED, 'https://github.com/dompdf/dompdf/blob/master/src/Options.php');

    return $application;
  }

  /**
   * Execute
   */
  public function execute(InputInterface $input, OutputInterface $output): int
  {
    $dompdfOptions = new Options([
      'chroot' => dirname($input->getArgument('input')),
      'isRemoteEnabled' => true,
    ]);

    $dompdfRootDir = dirname(__DIR__) . '/vendor/dompdf/dompdf';
    $dompdfOptions->setRootDir($dompdfRootDir);
    $dompdfOptions->setFontDir($dompdfRootDir . '/lib/fonts');
    $dompdfOptions->setFontCache($dompdfRootDir . '/lib/fonts');

    // parse and set options
    $options = explode(',', $input->getOption('options'));
    foreach ($options as $option) {
      $option = explode('=', $option);
      $dompdfOptions->set($option[0], $option[1] ?? true);
    }

    // load and render html file to output path
    $dompdf = new Dompdf($dompdfOptions);
    $dompdf->loadHtmlFile($input->getArgument('input'));
    $dompdf->render();
    file_put_contents($input->getArgument('output'), $dompdf->output());

    $output->writeln('Wrote PDF file to ' . $input->getArgument('output'));

    return Command::SUCCESS;
  }
}

